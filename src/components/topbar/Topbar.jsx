import React from 'react'
import './topbar.css'
import {NotificationsNone, Language, Settings} from '@material-ui/icons';
export default function Topbar() {
  return (
    <div className='topbar'>
      <div className='topbarWrapper'>
        <div className='topLeft'>
          <span className="logo">Admin</span>
        </div>
        <div className='topRight'>
          <div className="topbarIconContainer">
            <NotificationsNone />
            <span className="topIconBadge">2</span>
          </div>
          <div className="topbarIconContainer">
            <Language />
          </div>
          <div className="topbarIconContainer">
            <Settings />
          </div>
          <img src="https://media-exp1.licdn.com/dms/image/C5103AQE5m-qLezmM3g/profile-displayphoto-shrink_200_200/0/1587225904745?e=1657152000&v=beta&t=u8k3Xw249D0yqJAr6E0YaDZmPEUkOJOICuzwrJSjvX8" alt="" className="topAvtar" />
        </div>
      </div>
    </div>
  )
}
